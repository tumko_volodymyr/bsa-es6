
class ModalView {
  static titleElement = document.getElementById('modal-title');
  static bodyElement = document.getElementById('modal-body');

  static append = (...nodes) => {
    ModalView.bodyElement.append(...nodes);
  };
  static resetBody = () => {
    ModalView.bodyElement.innerHTML = '';
  };
  static resetTitle = () => {
    ModalView.setTitle('')
  };
  static  setTitle = title => {
    ModalView.titleElement.innerText = title;
  };
  static show = () => {
    $("#detailesModal").modal();
  }
}

export default ModalView;