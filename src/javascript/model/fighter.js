import { randomFloatBetweenMinMax } from '../services/randomUtils';

class Fighter {
    constructor(_id, name, health, attack, defense, source){
        this.name = name;
        this.health = health;
        this._originalHealth = health;
        this.attack = attack;
        this.defense = defense;
        this.source = source;
        this._id = _id;
    }

    getHitPower() {
        return this.attack * this.getChance();
    }

    getBlockPower() {
        return this.defense * this.getChance();
    }

    getChance(min=1, max=2){
        return randomFloatBetweenMinMax(min, max);
    }

    isAlive(){
        return this.health > 0;
    }

    punch(opponent){
        opponent.onPunch(this.getHitPower());
    }

    onPunch (power){
        let defend = this.getBlockPower();
        this.health = Math.max(0 ,(this.health - (Math.max(0, power - defend))));
    }

    getHealthPercent(){
        return Math.round(this.health/(this._originalHealth/100));
    }
}

export default Fighter;
