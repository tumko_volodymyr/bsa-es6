import View from './view';
import FighterView from './fighterView';
import DetailsView from './detailsView';
import FighterCreator from  './services/fighterCreator'

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    fighter = await  this.getFighterById(fighter._id);
    new DetailsView(this, fighter);
  }

  async getFighterById(fighterId){
    let fighter;
    if (undefined === (fighter = this.fightersDetailsMap.get(fighterId))){
      fighter = await FighterCreator.createById(fighterId);
      this.fightersDetailsMap.set(fighter._id, fighter);
    }
    return fighter;
  }

  updateFighter(fighter){
    this.fightersDetailsMap.set(fighter._id, fighter);
  }
}

export default FightersView;