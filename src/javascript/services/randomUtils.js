
function randomFloatBetweenMinMax(min, max) {
    return Math.random() * (max - min) + min;
}

export { randomFloatBetweenMinMax }