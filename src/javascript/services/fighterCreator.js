import Fighter from '../model/fighter';
import {fighterService} from "./fightersService";

class FighterCreator {
    static creatFromObject (obj) {
        const {_id, name, health, attack, defense, source} = obj;
        return new Fighter(_id, name, health, attack, defense, source);
    }

    static async createById (_id){
        const  fighter =  await  fighterService.getFighterDetails(_id);
        return  this.creatFromObject(fighter);
    }
}

export default FighterCreator;