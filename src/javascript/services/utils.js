
class Utils {
    static  clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        let copy = new obj.constructor();
        for (let attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }

    static delay = ms => new Promise(res => setTimeout(res, ms));
}

export default Utils;