class View {
  element;

  createElement({ tagName, className = '', attributes = {} }) {
    return View.createElement({ tagName:tagName, className: className, attributes:attributes });
  }

  static  createElement({ tagName, className = '', attributes = {} }) {
    const element = document.createElement(tagName);
    View.updateElement(element, {className: className, attributes: attributes});

    return element;
  }

  static  updateElement(element, {className = '', attributes = {} }) {
    element.className = '';
    for (let classStr of className.split(' ')){
      element.classList.add(classStr);
    }
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

export default View;
