import View from './view';
import ModalView from "./modalView";

class DetailsView extends View {
  handleInputTimeout;

  constructor(fightersView ,fighter) {
    super();
    this.fighter = fighter;
    this.fightersView = fightersView;
    this.handleInput = this.handlePropertyInput.bind(this);
    this.init();
  }

  init() {
    ModalView.setTitle(this.fighter.name);
    ModalView.resetBody();
    for (let property in this.fighter){
      if ( 0 === (property.indexOf('_')) || 'name' === property|| 'source' === property || 'progressBarView' === property ){
        continue;
      }
      let div = this.createElement({ tagName: 'div', className: 'form-group' });
      let label = this.createElement({ tagName: 'label', className: 'property-name' });
      label.innerText = property;

      let input = this.createElement({ tagName: 'input', className: 'form-control', attributes: {'data-name': property } });
      input.value = this.fighter[property];
      input.addEventListener('input', event => this.handleInput(event), false);
      div.append(label, input);
      ModalView.append(div);
    }
    ModalView.show();
  }

  updateFighterProperty (name, value){
    if (-1 === Object.keys(this.fighter).indexOf(name)){
      return;
    }
    this.fighter[name] = value;
    this.fightersView.updateFighter(this.fighter);
  }

  handlePropertyInput(event){
    let element = event.target;
    if(isNaN(element.value)){
      element.value  = this.fighter[element.getAttribute('data-name')];
      return;
    }
    if (undefined !== this.handleInputTimeout){
      clearTimeout(this.handleInputTimeout);
    }
    this.handleInputTimeout = setTimeout(element => {
      this.updateFighterProperty(element.getAttribute('data-name'), element.value)
    }, 1000, element);
  }
}

export default DetailsView;