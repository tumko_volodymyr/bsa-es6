import View from './view';
import FighterView from "./fighterView";
import Utils from "./services/utils";
import ModalView from "./modalView";
import ProgressBarView from "./progressBarView";

class ArenaView extends View {
  firstFighter;
  secondFighter;

  constructor(fighters, fightersView) {
    super();
    this.fighters = fighters;
    this.fightersView = fightersView;
    this.createView();
  }

  createView() {
    this.element = this.createElement({ tagName: 'div', className: 'container' });
    const row = View.createElement({ tagName: 'div', className: 'row'});
    const col1 = View.createElement({ tagName: 'div', className: 'col-sm-4 form-group'});
    col1.append(this.createFightersSelect('firstFighter'));
    const col2 = View.createElement({ tagName: 'div', className: 'col-sm-4'});
    col2.append(this.createMiddleColumn());
    const col3 = View.createElement({ tagName: 'div', className: 'col-sm-4 form-group'});
    col3.append(this.createFightersSelect('secondFighter'));
    row.append(col1, col2, col3);
    this.element.append(row);
  }

  async getFighterById(fighterId){
    if (isNaN(fighterId)){
      return undefined;
    }
    return await this.fightersView.getFighterById(fighterId);
  }

  createFightersSelect(fighter){
    const fighterOptions = this.fighters.map(fighter => {
      let option = View.createElement({ tagName: 'option', className: 'option', attributes: {'value': fighter._id }});
      option.innerText = fighter.name;
      return option
    });

    const select = View.createElement({ tagName: 'select', className: 'form-control'});
    const defOption = View.createElement({ tagName: 'option', className: 'option', attributes: {'value': '#', selected: true}});
    defOption.innerText = 'Choose fighter';
    select.append(defOption, ...fighterOptions);

    select.addEventListener('change',  event => this.handleChange(event, fighter), false);
    return select;
  }

  createMiddleColumn(){
    const div = View.createElement({ tagName: 'div', className: 'form-group text-center'});
    let btn = View.createElement({ tagName: 'button', className: 'btn btn-primary'});
    btn.innerText = 'Fight';
    btn.addEventListener('click', event => this.handleClick(event) , false);
    div.append(btn);
    return div;
  }

  async handleChange(event, fighter){
    this[fighter] = await this.getFighterById(event.target.value);
    const parentElement = event.target.parentNode;
    let last;
    while ( (last = parentElement.lastChild) && 'div' === last.nodeName.toLowerCase()) {
      parentElement.removeChild(last);
    }

    if (this[fighter]){
      const fighterView = new FighterView(this[fighter], () => {return} );
      const fighterViewElement = fighterView.element;
      const progressBarView = new ProgressBarView(this[fighter], event => {return});
      parentElement.append(progressBarView.element, fighterViewElement);
    }
  }

  async handleClick(event){
    if (!this.firstFighter || !this.secondFighter) {
      this.showFightersError();
      return;
    }
    let tempFirstFighter = Utils.clone(this.firstFighter);
    tempFirstFighter.progressBarView.fighter = tempFirstFighter;
    let tempSecondFighter = Utils.clone(this.secondFighter);
    tempSecondFighter.progressBarView.fighter = tempSecondFighter;
    let winner = await this.fight(tempFirstFighter, tempSecondFighter);
    this.showWinnerDetails(winner);
  }

  showFightersError(){
    ModalView.setTitle('No fighters selected');
    ModalView.resetBody();
    ModalView.append('Please choose fighters');
    ModalView.show();
  }

  showWinnerDetails(winner){
    ModalView.setTitle('The winner');
    ModalView.resetBody();
    ModalView.append(`Fighter ${winner.name} won in this fight`);
    ModalView.show();
  }

  async fight(firstFighter, secondFighter){
    if(firstFighter.isAlive() && secondFighter.isAlive()){
      firstFighter.punch(secondFighter);
      firstFighter.progressBarView.updateBar();
      secondFighter.progressBarView.updateBar();
      await  Utils.delay(400);
      return await this.fight(secondFighter, firstFighter);
    }else{
      return firstFighter.isAlive()?firstFighter:secondFighter;
    }
  }
}

export default ArenaView;