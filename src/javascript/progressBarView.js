import View from './view';

class ProgressBarView extends View {
  progressBar;
  fighter;

  constructor(fighter, handleClick) {
    super();
    this.fighter = fighter;
    this.createProgressBar(fighter, handleClick);
  }

  createProgressBar(fighter, handleClick) {
    fighter.progressBarView = this;
    this.progressBar = this.createBar(fighter);

    this.element = this.createElement({ tagName: 'div', className: 'progress' });
    this.element.append(this.progressBar);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createBar(fighter) {
    const health =  fighter.getHealthPercent();
    const attributes = { role: "progressbar", style: `width: ${health}%`, 'aria-valuenow': `${health}`,'aria-valuemin': "0", 'aria-valuemax': "100" };
    return this.createElement({
      tagName: 'div',
      className: 'progress-bar bg-success',
      attributes
    });
  }

  updateBar() {
    const health =  this.fighter.getHealthPercent();
    const attributes = { role: "progressbar", style: `width: ${health}%`, 'aria-valuenow': `${health}`,'aria-valuemin': "0", 'aria-valuemax': "100" };
    return View.updateElement(this.progressBar, {
      className: 'progress-bar bg-success',
      attributes
    });
  }
}

export default ProgressBarView;